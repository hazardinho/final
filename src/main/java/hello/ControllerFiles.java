package hello;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by eden on 5/13/17.
 */
@RestController
@RequestMapping("/final")
public class ControllerFiles {
    private String uploadDir = "/home/eden/final/";
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Autowired
    FilesRepository filesRepository;

    @RequestMapping(value = "addfile", method = RequestMethod.POST,headers=("content-type=multipart/*"))
    public ResponseEntity<String> addfiles(@RequestParam("file") MultipartFile file) throws Exception {
        String response = "";
        String newfilename="";
        String filename = file.getOriginalFilename();
        Date date = new Date();
        byte[] bytes = file.getBytes();
        InputStream newfile = file.getInputStream();
        if(filename.endsWith(".txt")){
            files asd = filesRepository.findByfilename(filename);
            if(asd!=null){
                newfilename = UUID.randomUUID().toString()+".txt";

            }
            else{
                newfilename = filename;
            }
            String filecontent = readFile(newfile);
            String pathupload = "";
            if(filecontent.toLowerCase().contains("spam")){
                File localfile = new File(uploadDir+"spam");
                if(!localfile.exists()){
                    localfile.mkdir();
                }
                pathupload = uploadDir+"spam/"+newfilename;
                Path path = Paths.get(pathupload);
                Files.write(path,bytes);

                response = "Spam found";
            }
            else{

                File localfile = new File(uploadDir+dateFormat.format(date));
                if(!localfile.exists()){
                    localfile.mkdir();
                }
                pathupload = uploadDir+dateFormat.format(date)+"/"+newfilename;
                Path path = Paths.get(pathupload);
                Files.write(path,bytes);

                response = "not spam";
            }

            files forUpload = new files(newfilename,bytes,date,pathupload);

            filesRepository.save(forUpload);
        }
        else{
            response = "File format should be txt";
        }

        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }
    @GetMapping(path="/spam/")
    public @ResponseBody
    ArrayList<files> getAllfiles(){
        ArrayList<files> all = new ArrayList<>();
        filesRepository.findAll().forEach(files -> {
            String[] arr = files.getFilepath().split("/");
            if(arr[4].equals("spam")){
                all.add(files);
            }
        });
        return all;
    }
    @DeleteMapping(path="/spam/")
    public @ResponseBody String deleteAllSpam(){
        filesRepository.findAll().forEach(files -> {
            String[] arr = files.getFilepath().split("/");
            if(arr[4].equals("spam")){
                filesRepository.delete(files);
                File localfile = new File(uploadDir+"spam/"+files.getFilename());
                localfile.delete();
            }
        });
        return "Deleted";
    }
    @GetMapping(path="/date/")
    public @ResponseBody ArrayList<files> getbyDate(@RequestParam("date") String date){
        ArrayList<files> asd = new ArrayList<>();
        filesRepository.findAll().forEach(files -> {
            String arr[] = files.getDateUpload().toString().split(" ");
            if(arr[0].equals(date)){
                asd.add(files);
            }
        });

        return asd;
    }

    public String readFile(InputStream inputStream) throws Exception{
        int ch;
        StringBuilder sb = new StringBuilder();
        while((ch = inputStream.read()) != -1) {
            sb.append((char) ch);
        }
        return sb.toString();
    }
}

