package hello;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by eden on 5/13/17.
 */

public interface FilesRepository extends CrudRepository<files, Long> {
    files findByfilename(String filename);
}
